# Getting started with kafka-node

## Kafka Producer basic Code

## Kafka Consumer basic Code

### Requirements:

[Install Apache Kafka](apache-kafka.md)

You cannot Run the Consumer and Producer code unless, you've followed [this steps](https://bitbucket.org/mohamedmhiri/node-kafka-getting-started/src/master/apache-kafka.md) up to creating a new topic.

### Create the Producer Api

[Producer Code](producer.md)

### Create the Consumer Api

[Consumer Code](consumer.md)