# INSTALL APACHE KAFKA

1. Download source code from
[Kafka Source Code](https://www.apache.org/dyn/closer.cgi?path=/kafka/version/kafka_version.tgz)

2. Decompress the downloaded source code for Unix like systems with

```
> tar -xzf kafka_version.tgz

> cd kafka_version
```
## Test the installation

### Run the server:
Start by running the ZooKeeper instance with the default configuration
```
> bin/zookeeper-server-start.sh config/zookeeper.properties
```
Open another terminal ( for Unix like systemes ) or an other command prompt ( for Windows systems ) then we run our kafka server with
```
> bin/kafka-server-start.sh config/server.properties
```
### Create a new topic
```
> bin/kafka-topic.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic topic-name
```
We can check our topics' list with
```
> bin/kafka-topics.sh --list --zookeeper localhost:2181
```
### Send messages

Run a producer with
```
> bin/kafka-console-producer.sh --broker-list localhost:9092 --topic topic-name
```
Then a consomer with
```
> bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic topic-name --from-beginning
```
#### We can now get all messages send by our producer in the consumer terminal/command prompt.
