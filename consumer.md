# Producer Code

This is a basic consumer boilerplate:

1. Create the Consumer

```
const kafka = require('kafka-node');
const HighLevelConsumer = kafka.HighLevelConsumer;
const consumerClient = new kafka.Client();
const consumer = new HighLevelConsumer(...);
consumer.on('message', (msg) => {
    ...
});

```
2. Create a property Object and add properties as following

```
const consumerPayload = {
    topic: 'topic-name',
    offset: 0,
    partition: 0
};
```
3. Basic Producer Message handler

```
consumer.on('message', (msg) => {
    console.log(msg);
});
```