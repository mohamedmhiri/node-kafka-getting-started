'use strict';

const kafka = require('kafka-node');
const HighLevelProducer = kafka.HighLevelProducer;
const client = new kafka.Client();
const producer = new HighLevelProducer(client);

const payload = {
    topic: 'topic-name',
    messages: ['get vouchers', 'get clients', 'get inventory'], // multi messages should be a array, single message can be just a string or a KeyedMessage instance
    key: 'omar', // only needed when using keyed partitioner
    partition: 0, // default 0
    attributes: 2, // default: 0
    timestamp: Date.now() // <-- defaults to Date.now() (only available with kafka v0.10 and KafkaClient only)
};

producer.on('ready', () => {
    console.log('producer running');
    producer.send([payload], (err, data) => {
        console.log(data);
    })
});
producer.on('error', (err) => {
    console.log(err);
});