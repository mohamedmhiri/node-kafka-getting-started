'use strict';

const kafka = require('kafka-node');
const HighLevelConsumer = kafka.HighLevelConsumer;
const consumerPayload = {
    /*
    feel free to change it
    */
    topic: 'topic-name',
    offset: 0,
    partition: 0
};

const consumerClient = new kafka.Client();
const consumer = new HighLevelConsumer(
    consumerClient,
    [
        consumerPayload
    ],
    {
        autoCommit: false
    },
);
consumer.on('message', (msg) => {
    console.log(msg);
});