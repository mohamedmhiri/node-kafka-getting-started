# Producer Code

This is a basic producer boilerplate:

1. Create the Producer

```
const kafka = require('kafka-node');
const HighLevelProducer = kafka.HighLevelProducer;
const client = new kafka.Client();
const producer = new HighLevelProducer(client);

producer.on('ready', () => {
    ...
});
producer.on('error', (err) => {
    ...
});

```
2. Create a property Object and add properties as following

```
const payload = {
    topic: 'topic-name',
    messages: ['get vouchers', 'get clients', 'get inventory'], // multi messages should be a array, single message can be just a string or a KeyedMessage instance
    key: 'omar', // only needed when using keyed partitioner
    partition: 0, // default 0
    attributes: 2, // default: 0
    timestamp: Date.now() // <-- defaults to Date.now() (only available with kafka v0.10 and KafkaClient only)
};
```
3. Basic Producer Ready handler

```
producer.on('ready', () => {
    console.log('producer running');
    producer.send([payload], (err, data) => {
        console.log(data);
    })
});
```
4. Basic Producer Error handler

```
producer.on('error', (err) => {
    console.log(err);
});
```